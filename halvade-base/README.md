# halvade-base

This Halvade-base image contains the binaries (bwa,samtools,gatk4,strelka2) and the spark/hadoop files.

## How to build the docker image:
```
docker build --no-cache -t halvade-base .
```
