# halvade-docker

When using the `halvade-docker.sh` script the required directories (reference dir, input files, output files etc) will automatically be mounted. All required binaries are already present inside the Docker image.

A work directory will be created in the folder you run the script from and the output file will be stored there.

### Synopsis:
```bash
halvade-docker.sh
  germline_pipeline: germline referenceFolder germlineIn [options]
  somatic_pipeline: somatic referenceFolder tumorIn normalIn [options]
  options:
    --exome # run the exome pipeline (less partitions)
    --tmpdir <string> # set the dir for tmp files
    --germlineSM <string> #: germline samplename
    --tumorSM <string> # tumor samplename
    --normalSM <string> # normal samplename
    --partitions <int> # set number of partitions

#Additional Halvade options can be set with the variable ${HALVADE_OPTS} like this:
HALVADE_OPTS="--variant_caller both"
#To override the automatically detected Yarn resources:
MEM=195 # in GB
CPUS=12 # num cores
```


## Pull the Docker image
When running the halvade-docker.sh the image will be automatically pulled but you can do it manually like this.
Either pull them from the Docker hub or build them yourself, see the `halvade` and `halvade-base` folders for further information.
To pull the latest image from the Docker hub, run the following command:
```bash
git pull ddecap/halvade:latest
```
