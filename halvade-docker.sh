#!/bin/bash

help="halvade-docker.sh help\n\
halvade-docker.sh germline referenceFolder germlineIn [options]\n\
halvade-docker.sh somatic referenceFolder tumorIn normalIn [options]\n\
options:\n\
\t--exome: run the exome pipeline (less partitions)\n\
\t--tmpdir <string>: set the dir for tmp files\n\
\t--germlineSM <string>: germline samplename\n\
\t--tumorSM <string>: tumor samplename\n\
\t--normalSM <string>: normal samplename\n\
\t--partitions <int>: set number of partitions\n\
\t--memory <int>: sets the available memory in GB\n\
\t--cpus <int>: sets the number of available CPUs\n\
\t--executor_memory <int>: sets the memory in MB per executor\n\
\t--executor_cpus <int>: sets the number of CPUs per executor\n\
Additional Halvade options can be set with the variable \${HALVADE_OPTS} like this:\n\
\tHALVADE_OPTS=\"--variant_caller both\""

if [[ "$#" == "0" ]]; then
  echo -e $help
  exit;
fi


red=$(tput -T xterm-256color setaf 1)
green=$(tput -T xterm-256color setaf 2)
reset=$(tput -T xterm-256color sgr0)

function configuredocker() {
  # parse arguments and mount directories
  MOUNTS=""
  if [[ -z $UUID ]]; then UUID=$(date +%s%3N); fi
  # MOUNT WORKDIR
  if [[ -z $WORKDIR ]]; then
    WORKDIR="$(pwd)/docker-workdir-$UUID";
    mkdir $WORKDIR
    MOUNTS="$MOUNTS -v $WORKDIR:/usr/local/halvade/workdir"
    DOCKERWORKDIR="/usr/local/halvade/workdir"
  fi
  # ADDITIONAL MOUNTS FOR REFERENCE/INPUT/TMP FOLDERS
  declare -A MOUNTED_FOLDERS
  MOUNT_ID=0
  if [[ ! -z $GERMLINE ]]; then
    GERMLINE_DIR=$(dirname $GERMLINE)
    if [ ! -v MOUNTED_FOLDERS[$GERMLINE_DIR] ]; then # not yet mounted
      MOUNTED_FOLDERS[$GERMLINE_DIR]="/usr/local/halvade/mount_${MOUNT_ID}"
      MOUNTS="$MOUNTS -v $GERMLINE_DIR:${MOUNTED_FOLDERS[$GERMLINE_DIR]}"
      MOUNT_ID=$((MOUNT_ID + 1))
    fi
    GERMLINE="${GERMLINE/$GERMLINE_DIR/${MOUNTED_FOLDERS[$GERMLINE_DIR]}}"
  fi
  if [[ ! -z $TUMOR_IN ]]; then
    TUMOR_IN_DIR=$(dirname $TUMOR_IN)
    if [ ! -v MOUNTED_FOLDERS[$TUMOR_IN_DIR] ]; then # not yet mounted
      MOUNTED_FOLDERS[$TUMOR_IN_DIR]="/usr/local/halvade/mount_${MOUNT_ID}"
      MOUNTS="$MOUNTS -v $TUMOR_IN_DIR:${MOUNTED_FOLDERS[$TUMOR_IN_DIR]}"
      MOUNT_ID=$((MOUNT_ID + 1))
    fi
    TUMOR_IN="${TUMOR_IN/$TUMOR_IN_DIR/${MOUNTED_FOLDERS[$TUMOR_IN_DIR]}}"
  fi
  if [[ ! -z $NORMAL_IN ]]; then
    NORMAL_IN_DIR=$(dirname $NORMAL_IN)
    if [ ! -v MOUNTED_FOLDERS[$NORMAL_IN_DIR] ]; then # not yet mounted
      MOUNTED_FOLDERS[$NORMAL_IN_DIR]="/usr/local/halvade/mount_${MOUNT_ID}"
      MOUNTS="$MOUNTS -v $NORMAL_IN_DIR:${MOUNTED_FOLDERS[$NORMAL_IN_DIR]}"
      MOUNT_ID=$((MOUNT_ID + 1))
    fi
    NORMAL_IN="${NORMAL_IN/$NORMAL_IN_DIR/${MOUNTED_FOLDERS[$NORMAL_IN_DIR]}}"
  fi
  if [[ ! -z $TMP_DIR ]]; then
    if [ -f $TMP_DIR ]; then TMP_DIR=$(dirname $TMP_DIR); fi
    if [ ! -v MOUNTED_FOLDERS[$TMP_DIR] ]; then # not yet mounted
      MOUNTED_FOLDERS[$TMP_DIR]="/usr/local/halvade/mount_${MOUNT_ID}"
      MOUNTS="$MOUNTS -v $TMP_DIR:${MOUNTED_FOLDERS[$TMP_DIR]}"
      MOUNT_ID=$((MOUNT_ID + 1))
    fi
    TMP_DIR=${MOUNTED_FOLDERS[$TMP_DIR]}
  fi
  if [[ ! -z $REF_DIR ]]; then
    if [ -f $REF_DIR ]; then REF_DIR=$(dirname $REF_DIR); fi
    if [ ! -v MOUNTED_FOLDERS[$REF_DIR] ]; then # not yet mounted
      MOUNTED_FOLDERS[$REF_DIR]="/usr/local/halvade/mount_${MOUNT_ID}"
      MOUNTS="$MOUNTS -v $REF_DIR:${MOUNTED_FOLDERS[$REF_DIR]}"
      MOUNT_ID=$((MOUNT_ID + 1))
    fi
    REF_DIR=${MOUNTED_FOLDERS[$REF_DIR]}
  fi
  # EXTRA OPTIONS
  if [[ ! -z $HALVADE_OPTS ]]; then # only add when non Empty
    ENV_FILE=$WORKDIR/docker.env
    echo "HALVADE_OPTS=$HALVADE_OPTS" > $ENV_FILE
    DOCKER_ENV="--env-file $ENV_FILE"
  fi
  # DATA FROM DOCKER
  BIN_DIR="/usr/local/halvade/"
  DOCKER_IMAGE="ddecap/halvade"
  # OVERRIDE RESOURCES
  RESOURCES=""
  if [[ ! -z $RESOURCE_MEMORY ]]; then RESOURCES="$RESOURCES --memory $RESOURCE_MEMORY"; fi
  if [[ ! -z $RESOURCE_CPU ]]; then RESOURCES="$RESOURCES --cpus $RESOURCE_CPU"; fi
}

function parseOptions() {
  # parse options
  while [[ $# -gt 0 ]]
  do
    key="$1"
    case $key in
        --exome)
          EXOME_PIPELINE="true"
          shift # past argument
          ;;
        --germlineSM)
          SM="$2"
          shift # past argument
          shift # past value
          ;;
        --executor_memory)
          EXECUTOR_MEMORY="$2"
          shift # past argument
          shift # past value
          ;;
        --executor_cpus)
          TASK_CPUS="$2"
          shift # past argument
          shift # past value
          ;;
        --partitions)
          PARTITIONS="$2"
          shift # past argument
          shift # past value
          ;;
        --tmpdir)
          TMP_DIR="$2"
          shift # past argument
          shift # past value
          ;;
        --outputfolder)
          OUTPUTFOLDER="$2"
          shift # past argument
          shift # past value
          ;;
        --tumorSM)
          TUMOR_SM="$2"
          shift # past argument
          shift # past value
          ;;
        --normalSM)
          NORMAL_SM="$2"
          shift # past argument
          shift # past value
          ;;
        --memory)
          RESOURCE_MEMORY="$2"
          shift # past argument
          shift # past value
          ;;
        --cpus)
          RESOURCE_CPU="$2"
          shift # past argument
          shift # past value
          ;;
        *)    # unknown option
          echo "unknown option $1"
          exit 100;
          ;;
    esac
  done
}

# read arguments and process
MODE=$1
shift
if [[ "$MODE" = "germline" ]]; then
  if [[ "$#" -lt "2" ]]; then
    echo -e $help
    exit
  fi
  CLASS=be.ugent.intec.halvade.job.GermlinePipeline
  REF_DIR=$(readlink -f $1); shift;
  GERMLINE=$(readlink -f $1); shift;
  SM="SAMPLE"
  parseOptions "$@"
  if [[ -z "$PARTITIONS" ]]; then
    if [[ -z "$EXOME_PIPELINE" ]]; then
      if [[ $INPUT == *bam ]]; then PARTITIONS=1800; else PARTITIONS=1500; fi
    else
      if [[ $INPUT == *bam ]]; then PARTITIONS=320; else PARTITIONS=270; fi
    fi
  fi
  configuredocker
  HALVADE_ARGUMENTS="--germline $REF_DIR $BIN_DIR $GERMLINE $DOCKERWORKDIR"
  if [[ ! -z $TMP_DIR ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --tmpdir $TMP_DIR"; fi
  if [[ ! -z $PARTITIONS ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --partitions $PARTITIONS"; fi
  if [[ ! -z $EXOME_PIPELINE ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --exome"; fi
  if [[ "$SM" != "SAMPLE" ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --germlineSM $SM"; fi
  if [[ ! -z "$RESOURCE_MEMORY" ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --memory $RESOURCE_MEMORY"; fi
  if [[ ! -z "$RESOURCE_CPU" ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --cpus $RESOURCE_CPU"; fi
  if [[ ! -z "$EXECUTOR_MEMORY" ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --executor_memory $EXECUTOR_MEMORY"; fi
  if [[ ! -z "$TASK_CPUS" ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --executor_cpus $TASK_CPUS"; fi
  RESULT="germline*"

elif [[ "$MODE" = "somatic" ]]; then
  if [[ "$#" -lt "3" ]]; then
    echo -e $help
    exit
  fi
  CLASS=be.ugent.intec.halvade.job.SomaticPipeline
  REF_DIR=$(readlink -f $1); shift;
  TUMOR_IN=$(readlink -f $1); shift;
  NORMAL_IN=$(readlink -f $1); shift;
  TUMOR_SM="TUMOR_SM"
  NORMAL_SM="NORMAL_SM"
  parseOptions "$@"
  if [[ -z "$PARTITIONS" ]]; then
    if [[ -z "$EXOME_PIPELINE" ]]; then
      if [[ $TUMOR_IN == *bam || $NORMAL_IN == *bam ]]; then PARTITIONS=1800; else PARTITIONS=1500; fi
    else
      if [[ $TUMOR_IN == *bam || $NORMAL_IN == *bam ]]; then PARTITIONS=320; else PARTITIONS=270; fi
    fi
  fi
  configuredocker
  HALVADE_ARGUMENTS="--somatic $REF_DIR $BIN_DIR $TUMOR_IN $NORMAL_IN $DOCKERWORKDIR"
  if [[ ! -z $TMP_DIR ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --tmpdir $TMP_DIR"; fi
  if [[ ! -z $PARTITIONS ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --partitions $PARTITIONS"; fi
  if [[ ! -z $EXOME_PIPELINE ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --exome"; fi
  if [[ "$TUMOR_SM" != "TUMOR_SM" ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --tumorSM $TUMOR_SM"; fi
  if [[ "$NORMAL_SM" != "NORMAL_SM" ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --normalSM $NORMAL_SM"; fi
  if [[ ! -z "$RESOURCE_MEMORY" ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --memory $RESOURCE_MEMORY"; fi
  if [[ ! -z "$RESOURCE_CPU" ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --cpus $RESOURCE_CPU"; fi
  if [[ ! -z "$EXECUTOR_MEMORY" ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --executor_memory $EXECUTOR_MEMORY"; fi
  if [[ ! -z "$TASK_CPUS" ]]; then HALVADE_ARGUMENTS="$HALVADE_ARGUMENTS --executor_cpus $TASK_CPUS"; fi
  RESULT="somatic*"
else
  echo -e $help
  exit
fi

if [[ $(groups | grep -c docker) == "0" ]]; then
  echo "${red}you do not seem to have docker permission, please run this command:${reset}"
  echo "sudo docker run ${MOUNTS} ${DOCKER_USER} ${DOCKER_ENV} ${DOCKER_IMAGE} ${RESOURCES} ${HALVADE_ARGUMENTS}"
  echo "output will be generated at ${green}${WORKDIR}/${RESULT}${reset}"
else
  docker run ${MOUNTS} ${DOCKER_USER} ${DOCKER_ENV} ${DOCKER_IMAGE} ${RESOURCES} ${HALVADE_ARGUMENTS}
  RESULT_FILES=$(find ${WORKDIR} -name ${RESULT} -not -name *regions)
  if [ ! -z "${RESULT}" ]; then
    if [[ ! -z "${OUTPUTFOLDER}" ]]; then
      mkdir -p ${OUTPUTFOLDER}
      # move data to outputfolder
      echo "output: ${green}"
      for file in ${RESULT_FILES}; do
        filename="${file##*/}"
        extension="${filename#*.}"
        mv ${file} $OUTPUTFOLDER/${TUMOR_SM}_vs_${NORMAL_SM}.${extension}
        echo "$OUTPUTFOLDER/${TUMOR_SM}_vs_${NORMAL_SM}.${extension}"
      done
      echo "${reset}"
    else
      echo "output: ${green}${RESULT_FILES}${reset}";
    fi
  else
    echo "output ${red}${WORKDIR}/${RESULT}${reset} not found, an error has occured."
  fi
fi
