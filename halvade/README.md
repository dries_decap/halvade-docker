# Halvade Docker
With this image you can run the Halvade germline/somatic variant calling pipeline. This image is intended for WXS use but can also be used with WGS samples with the `--wgs` flag.
## How to build the docker image:
This docker image is based on the halvade-base image, which should be build first. See README.md in the halvade-base folder.
```
docker build --no-cache -t halvade .
```

## How to start the docker interactively
```
docker run -it halvade --bash
```
