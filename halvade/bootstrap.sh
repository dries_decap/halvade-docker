#!/bin/bash

help="commands:\n -d|--daemon\tactivate it as a daemon, to which you can connect with docker later\n\
 -b|--bash\tstart an interactive bash shell\n\
 -h|--help\tshow help\n\
 --halvade\trun halvade and exit;\n\
halvade.sh synopsis:\n\
  germline: germline referenceFolder germlineIn outputFolder [options]\n\
  somatic: somatic referenceFolder tumorIn normalIn outputFolder [options]\n\
  options:\n\
  \t--exome: run the exome pipeline (less partitions)\n\
  \t--germlineSM: germline samplename\n\
  \t--tumorSM: tumor samplename\n\
  \t--normalSM: normal samplename\n\
  Additional Halvade options can be set with the variable \${HALVADE_OPTS} like this:\n\
  \tHALVADE_OPTS=\"--variant_caller both\"\n\
extra arguments:\n\
  -m|--mem\tsets the memory of the machine\n\
  -c|--cpus\tsets the cpus of the machine"

INPUTS=()
while [[ $# -gt 0 ]]
do
  key="$1"
  case $key in
      -m|--memory)
        RESOURCE_MEMORY="$2" # in GB
        RESOURCE_MEMORY=$((RESOURCE_MEMORY*1024)) # in MB
        shift # past argument
        shift # past value
        ;;
      -c|--cpus)
        RESOURCE_CPU="$2"
        shift # past argument
        shift # past value
        ;;
      -h|--help)
        echo -e $help
        shift #past argument
        exit; # exit command
        ;;
      -b|--bash)
        cmd="/bin/bash"
        shift # past argument
        ;;
      -d|--daemon)
        cmd="while true; do sleep 1000; done"
        shift # past argument
        ;;
      --germline)
        shift # past argument
        INPUTS+=( "$2" )
        cmd="/usr/local/halvade/halvade.sh germline $*"
        break
        # unset "$@"
        ;;
      --somatic)
        shift # past argument
        INPUTS+=( "$2" "$3" )
        cmd="/usr/local/halvade/halvade.sh somatic $*"
        break
        # unset "$@"
        ;;
      *)    # unknown option
        echo "unknown option $1"
        shift # past argument
        ;;
  esac
done


## configure HADOOP
HOSTNAME=$(hostname -i)
if [[ -z $DEFAULT_FS ]]; then
  if [[ -z $USE_HDFS ]]; then
    DEFAULT_FS="hdfs://$HOSTNAME:9000"
  else
    DEFAULT_FS="file:///"
  fi
fi
if [[ -z $WORKDIR ]]; then WORKDIR="/usr/local/halvade/workdir"; fi
mkdir -p "${WORKDIR}"
if [[ -z $DFS_NAMENODE ]]; then
  DFS_NAMENODE="${WORKDIR}/nameNode"
fi
if [[ -z $DFS_DATANODE ]]; then
  DFS_DATANODE="${WORKDIR}/dataNode"
fi
if [[ -z $LOG_DIRECTORY ]]; then
  LOG_DIRECTORY="${WORKDIR}/logs"
fi
if [[ -z $LOCAL_DIRS ]]; then
  LOCAL_DIRS="${WORKDIR}/nm-local-dir"
fi
if [[ -z $DFS_REPLICATION ]]; then
  DFS_REPLICATION=1
fi
if [[ -z $RESOURCE_MEMORY ]]; then
  MEM_KB=$(grep MemTotal /proc/meminfo | awk '{print $2}') # in KB
  RESOURCE_MEMORY=$((MEM_KB/1024)) # in MB
fi
if [[ -z $MAXIMUM_MEMORY ]]; then
  MAXIMUM_MEMORY=$RESOURCE_MEMORY
fi
if [[ -z $MINIMUM_MEMORY ]]; then
  MINIMUM_MEMORY=1024
fi
if [[ -z $RESOURCE_CPU ]]; then
  RESOURCE_CPU=$(grep -c processor /proc/cpuinfo) # get cores instead of threads!
fi
if [[ -z $MAXIMUM_CPU ]]; then
  MAXIMUM_CPU=$RESOURCE_CPU
fi
if [[ -z $JAVA_HOME ]]; then
  JAVA_HOME=$(readlink -f $(which java) | sed "s/\/bin\/java//g")
fi
if [[ -z $SPARK_SHUFFLE_JAR ]]; then
  SPARK_SHUFFLE_JAR=$(ls $SPARK_HOME/yarn/spark*-yarn-shuffle.jar)
fi

HADOOP_CONF_DIR=${HADOOP_CONF_DIR:=/usr/local/hadoop/etc/hadoop}
# core-site.xml
sed -i "s#DEFAULT_FS#$DEFAULT_FS#g" $HADOOP_CONF_DIR/core-site.xml
# hdfs-site.xml
sed -i "s#DFS_NAMENODE#$DFS_NAMENODE#g" $HADOOP_CONF_DIR/hdfs-site.xml # not changed
sed -i "s#DFS_DATANODE#$DFS_DATANODE#g" $HADOOP_CONF_DIR/hdfs-site.xml # not changed
sed -i "s#DFS_REPLICATION#$DFS_REPLICATION#g" $HADOOP_CONF_DIR/hdfs-site.xml
# yarn-site.xml
sed -i "s#SPARK_SHUFFLE_JAR#$SPARK_SHUFFLE_JAR#g" $HADOOP_CONF_DIR/yarn-site.xml
sed -i "s#RESOURCE_MEMORY#$RESOURCE_MEMORY#g" $HADOOP_CONF_DIR/yarn-site.xml
sed -i "s#MAXIMUM_MEMORY#$MAXIMUM_MEMORY#g" $HADOOP_CONF_DIR/yarn-site.xml
sed -i "s#MINIMUM_MEMORY#$MINIMUM_MEMORY#g" $HADOOP_CONF_DIR/yarn-site.xml
sed -i "s#RESOURCE_CPU#$RESOURCE_CPU#g" $HADOOP_CONF_DIR/yarn-site.xml
sed -i "s#MAXIMUM_CPU#$MAXIMUM_CPU#g" $HADOOP_CONF_DIR/yarn-site.xml
sed -i "s#LOG_DIRECTORY#$LOG_DIRECTORY#g" $HADOOP_CONF_DIR/yarn-site.xml # not changed
sed -i "s#LOCAL_DIRS#$LOCAL_DIRS#g" $HADOOP_CONF_DIR/yarn-site.xml  # not changed
sed -i "s#HOSTNAME#$HOSTNAME#g" $HADOOP_CONF_DIR/yarn-site.xml
sed -i "s#export JAVA_HOME=\${JAVA_HOME}#export JAVA_HOME=$JAVA_HOME#" $HADOOP_CONF_DIR/hadoop-env.sh
# set slaves
echo "$HOSTNAME" > $HADOOP_CONF_DIR/slaves
#sshd
/usr/bin/ssh-keygen -A
mkdir -p /run/sshd
/usr/sbin/sshd -E /tmp/sshd.log
ssh-keyscan -H "$HOSTNAME" 2>/dev/null >> /root/.ssh/known_hosts

# export env
export HOSTNAME WORKDIR JAVA_HOME RESOURCE_MEMORY RESOURCE_CPU

# start yarn
"$HADOOP_PREFIX"/sbin/start-yarn.sh

# run command!
cd $WORKDIR
$cmd

if [[ ! -z $INPUTS ]]; then # CLEANUP IF NOT INTERACTIVE BASH
  # MAKE SURE ALL DATA FROM ROOT IS ACCESSIBLE
  chmod -R 777 ${WORKDIR}
  for INPUT in ${INPUTS[@]}; do
    if [ -d ${INPUT}/preprocessed ]; then
      chmod -R 777 ${INPUT}/preprocessed
    fi
  done

  # STOP YARN
  "$HADOOP_PREFIX"/sbin/stop-yarn.sh
  # CLEANUP LOCAL DIRS
  rm -r $LOCAL_DIRS
fi
